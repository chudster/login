package login;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import LoginSys.RegSys;

public class Gui extends Main implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JMenuBar menuBar;
	private JPanel actionPanel, loginPanel, regPanel, userPanel;
	private JMenu menuFile, menuOptions, menuHelp; 
	private JMenuItem mClose;
	private JTextField tLogin, tPassword, tPasswordConf;
	private JButton bLog, bReg, bLogin;
	public Gui() {
		frame =  new JFrame("Login");
		frame.setSize(200, 200	);
		frame.setLayout(new GridLayout(3, 1));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLookAndFeel();
		menuBar();
		actionPanel();
		loginPanel();
		regPanel();
		userPanel();
		frame.pack();
		frame.setVisible(true);
		
		
		
	}
	
	private static void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		}
		catch (Exception exc) {
			//ignore an error
		}
	}
	
	private void menuBar()
	{
		
		menuBar = new JMenuBar();
		menuFile = new JMenu("Plik");
		menuOptions = new JMenu("Opcje");
		menuHelp = new JMenu("Pomoc");
		//menus add
		menuBar.add(menuFile);
		menuBar.add(menuOptions);
		menuBar.add(menuHelp);
		//menuItems 
		mClose = new JMenuItem("Zakończ");
		mClose.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				    System.exit(0);
				  } 
				} );
		//menuItem add
		menuFile.addSeparator();
		menuFile.add(mClose);
		
		frame.setJMenuBar(menuBar);
		
		
		
	}
	
	private void actionPanel() {
		actionPanel = new JPanel();
		actionPanel.setBackground(Color.yellow);
		actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.Y_AXIS));
		
		bLog = new JButton("Logowanie");
		bLog.setAlignmentX(CENTER_ALIGNMENT);
		bLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.remove(regPanel);
				frame.add(loginPanel);
				frame.revalidate();
				frame.repaint();
				frame.pack();

			}
		});
		
		bReg = new JButton("Rejestracja");
		bReg.setAlignmentX(CENTER_ALIGNMENT);
		bReg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.remove(loginPanel);
				frame.add(regPanel);
				frame.revalidate();
				frame.repaint();
				frame.pack();
			}
		});
		
		actionPanel.add(bLog);
		actionPanel.add(bReg);
		pack();
		frame.add(actionPanel);
	}
	
	private void regPanel() {
		regPanel = new JPanel();
		regPanel.setBackground(Color.blue);
		regPanel.setLayout(new BoxLayout(regPanel, BoxLayout.Y_AXIS));
		
		JLabel lLogin = new JLabel("Login");
		lLogin.setAlignmentX(CENTER_ALIGNMENT);
		JLabel lPassword = new JLabel("Hasło");
		lPassword.setAlignmentX(CENTER_ALIGNMENT);
		JLabel lPasswordConf = new JLabel("Powtórz hasło");
		lPasswordConf.setAlignmentX(CENTER_ALIGNMENT);
		tLogin = new JTextField(5);
		tPassword = new JTextField(5);
		tPasswordConf = new JTextField(5);
		
		JButton bReg = new JButton("Rejestruj");
		bReg.setAlignmentX(CENTER_ALIGNMENT);
		bReg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegSys regSys = new RegSys();
				regSys.insertUser(tLogin.getText(), tPassword.getText(), tPasswordConf.getText());
			}
		});
		
		regPanel.add(lLogin);
		regPanel.add(tLogin);
		regPanel.add(lPassword);
		regPanel.add(tPassword);
		regPanel.add(lPasswordConf);
		regPanel.add(tPasswordConf);
		regPanel.add(bReg);
		pack();
		
		
	}
	
	private void loginPanel() {
		loginPanel = new JPanel();
		loginPanel.setBackground(Color.MAGENTA);
		loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
		
		JLabel lLogin = new JLabel("Login");
		lLogin.setAlignmentX(CENTER_ALIGNMENT);
		JLabel lPassword = new JLabel("Hasło");
		lPassword.setAlignmentX(CENTER_ALIGNMENT);
		tLogin = new JTextField(5);
		tPassword = new JTextField(5);
		bLogin = new JButton("Zaloguj");
		bLogin.setAlignmentX(CENTER_ALIGNMENT);
		
		loginPanel.add(lLogin);
		loginPanel.add(tLogin);
		loginPanel.add(lPassword);
		loginPanel.add(tPassword);
		loginPanel.add(bLogin);
		pack();
		
		
	}
	
	private void userPanel() {
		userPanel = new JPanel();
		userPanel.setBackground(Color.darkGray);
		userPanel.setLayout(new BoxLayout(userPanel, BoxLayout.Y_AXIS));
		
		JLabel lUserPanel = new JLabel("Panel");
		lUserPanel.setAlignmentX(CENTER_ALIGNMENT);
		
		userPanel.add(lUserPanel);
		frame.add(userPanel);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public String getUserName() {
		return tLogin.getText();
	}
	
	public String getPassword() {
		return tPassword.getText();
	}

}
