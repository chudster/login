package LoginSys;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class dbConnect {
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:users.db";
	
	public Connection conn;
	public Statement stmt;
	
	public dbConnect() {
		try {
			Class.forName(dbConnect.DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println("No JDBC Driver");
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection(DB_URL);
			stmt = conn.createStatement();
		} catch (SQLException e) {
			System.err.println("Problem with connection");
			e.printStackTrace();
		}
		createTables();
	}
	
	public boolean createTables() {
		String createUsers = "CREATE TABLE IF NOT EXISTS users (id_user INTEGER PRIMARY KEY AUTOINCREMENT, username varchar(255), password varchar(255))";
		try {
			stmt.execute(createUsers);
		} catch (SQLException e) {
			System.err.println("Table creation error");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	   public void closeConnection() {
	        try {
	            conn.close();
	        } catch (SQLException e) {
	            System.err.println("Problem z zamknieciem polaczenia");
	            e.printStackTrace();
	        }
	    }
}
