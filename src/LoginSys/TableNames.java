package LoginSys;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.transform.Result;

public class TableNames {
	
	public ArrayList<String> arrTblNames;
	
	public TableNames() {
		arrTblNames = new ArrayList<String>();
		dbConnect dbc = new dbConnect();
		
		String query = "SELECT name FROM users WHERE type='table'";
		
		try {
			PreparedStatement prepStmt = dbc.conn.prepareStatement(query);
			ResultSet res = prepStmt.executeQuery();
			
			while (res.next()) {
				arrTblNames.add(res.getCursorName());
			}
			
			
		} catch (SQLException e) {
			
		}
		
		
		
		
		
	}
	
}
