package LoginSys;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class RegSys {
	
	
	
	public RegSys() {
		
	}
	


	public boolean insertUser (String username, String password, String passwordConf) {
		
		if (password.equals(passwordConf))
		{
			dbConnect dbc = new dbConnect();
			try {
				PreparedStatement prepStmt = dbc.conn.prepareStatement(
						"insert into users values (NULL, ?, ?);");
				prepStmt.setString(1, username);
				prepStmt.setString(2, password);
				prepStmt.executeUpdate();
				prepStmt.close();
			} catch (SQLException e) {
				System.err.println("User registration: problem occured");
				e.printStackTrace();
				return false;
			}
			dbc.closeConnection();
			JOptionPane.showMessageDialog(null, "Rejestracja użytkownika: "+username+" przebiegła pomyślnie");
			return true;
		}
		else
			JOptionPane.showMessageDialog(null, "Podane hasła nie są tożsame");
		
		return false;
	}
	
 

}
	