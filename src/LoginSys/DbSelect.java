package LoginSys;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class DbSelect {

	public DbSelect() {
		
	}
	
	public LinkedList<Columns> select(String tableName, String column1, String column2, String column3) {
		LinkedList<Columns> dbSelect =  new LinkedList<Columns>();
		dbConnect dbc = new dbConnect();
		try {
			String Query = "SELECT * FROM "+ tableName + " ";
			PreparedStatement statment = dbc.conn.prepareStatement(Query);
			ResultSet res = statment.executeQuery();
			
	
			String resColumn1, resColumn2, resColumn3;
			
			while(res.next()) {
				resColumn1 = res.getString(column1);
				resColumn2 = res.getString(column2);
				resColumn3 = res.getString(column3);
				dbSelect.add(new Columns(resColumn1, resColumn2, resColumn3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return dbSelect;

	}
	
    public void closeConnection() {
        try {
        	dbConnect dbc = new dbConnect();
            dbc.conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
	
}
